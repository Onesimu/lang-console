// import { srct, dett } from './lsx.js'

const srct = it => it.e(/^\s*[\/]{2}/gm, '#')
.e(/".*?\$.*?"/gm, i => i.e('\\$', '#'))
.e(/^(\s*if .+?) rn([^\n]*)$/gm, '$1 then return $2')
.e(/\brn\b/gm, 'return')
.e(/^\s*(import .+)$/gm, '\n``$1``\n')
.e(/^\s*(export {.+)$/gm, '\n``$1``\n')
.e(/^\s*(export default)/gm, '\n``$1``')
            
const dett = it => it.e(/;$/gm, '').e(/:\s*function/g, '')
.e(/function\s*\((.*)\)/g, '($1) => ')
.e(/\((\w+)\) => /g, '$1 => ')
.e(/=>\s*?\{\s*?return ([\S ]+)\s*?\}\)/g, '=> $1)')
.e(/\{\n\s*(.+)\n\s*\}/g, '{ $1 }')

var tabId = chrome.devtools?.inspectedWindow.tabId
var err = document.getElementById('error')

var editor = ace.edit("cc-editor")
editor.setTheme("ace/theme/clouds")
editor.session.setMode("ace/mode/livescript")
editor.session.setUseSoftTabs(true)
editor.session.setUseWrapMode(true)
editor.session.setTabSize(2)
editor.setShowPrintMargin(false)

var compiled = ace.edit("cc-results")
compiled.setTheme("ace/theme/clouds")
compiled.session.setMode("ace/mode/javascript")
compiled.session.setUseSoftTabs(true)
compiled.session.setUseWrapMode(true)
compiled.session.setTabSize(2)
compiled.setShowPrintMargin(false)

const log = (...args) => chrome.devtools.inspectedWindow.eval(`
  console.log(...${JSON.stringify(args)});
`)
String.prototype.e = String.prototype.replace;

function run(){
    chrome.devtools.inspectedWindow["eval"](compiled.session.getValue(), function(result, exception) {
      if (exception && (exception.isError || exception.isException)) {
          if (exception.isError) {
            err.className = ''
            err.innerHTML = "Error " + exception.code + ": " + exception.description
          }
          if (exception.isException) {
            err.className = ''
            err.innerHTML = "Exception: " + exception.value
          }
        }
      else {
          err.className = 'green'
          const res = result instanceof Node ? result.innerHTML : JSON.stringify(result)
          err.innerHTML = res
          log(res)
        }
    })
}

function compileIt(){
  try {
    const src = srct(editor.session.getValue())
    console.log(src)
    LiveScript = require('livescript')
    var compiledSource = LiveScript.compile(src, {bare:true, header: false, const: 0})
    // var compiledSource = coffee2ls.ls2js(src)
    var dest = dett(compiledSource)
    compiled.session.setValue(dest)
    err.className = 'is-hidden'
  } catch (error) {
    err.className = ''
    err.innerHTML = error.message
  }
  // localStorage.setItem("state" + tabId, src)
  // run()
}

function decompileIt(){
    try {
        var src = compiled.session.getValue()
        var output = Babel.transform(src, { presets: ["env"] }).code
        console.log(output)
        var res = js2coffee(output)
        console.log(res)
        res = coffee2ls.compile(coffee2ls.parse(res))
        // res = coffee2ls.ls(res)
        editor.session.setValue(res)
        err.className = 'is-hidden'
    } catch (error) {
        err.className = ''
        err.innerHTML = error.message
    }
    // localStorage.setItem("state" + tabId, editor.session.getValue())
    // run()
}

var compileOptions = {
    name: "compileIt",
    exec: compileIt,
    bindKey: "Ctrl-Return"
}

var decompile = {
    name: "decompileIt",
    exec: decompileIt,
    bindKey: "Ctrl-Return"
}

// editor.commands.addCommand(decompile)
editor.commands.addCommand(compileOptions)
compiled.commands.addCommand(decompile)
// compiled.commands.addCommand(compileOptions)

document.getElementById('runcc').addEventListener('click', run)
// tabId && editor.session.setValue(localStorage.getItem("state" + tabId))
